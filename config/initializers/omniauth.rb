require 'openid/store/memcache'

ExpenseTracker::Application.middleware.use OmniAuth::Builder do
  provider :open_id, name: 'google', identifier: 'https://www.google.com/accounts/o8/id', store: OpenID::Store::Memcache.new(Rails.cache.instance_eval("@data"))
end
