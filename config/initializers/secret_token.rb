# Be sure to restart your server when you modify this file.

# Your secret key for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!
# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
ExpenseTracker::Application.config.secret_token = if Rails.env.production?
  ENV['SECRET_TOKEN']
else
  '37ae1aa1774dd3dc7323ed14e14b15d0aec881cb989ddd8ae7dcae6d91541db4717c72c6989329fbe93eee5053b96b3fb2996e14c6648ddcf3bc393f4da9bc0a'
end
