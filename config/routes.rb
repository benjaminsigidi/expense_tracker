ExpenseTracker::Application.routes.draw do
  root to: redirect("/expenses")

  resources :expenses

  get "/login", to: redirect("/auth/google")
  match "/auth/:provider/callback", to: 'sessions#create'

end
