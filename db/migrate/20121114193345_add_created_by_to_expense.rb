class AddCreatedByToExpense < ActiveRecord::Migration
  def change
    change_table :expenses do |t|
      t.references :created_by
      t.references :updated_by
    end
  end
end
