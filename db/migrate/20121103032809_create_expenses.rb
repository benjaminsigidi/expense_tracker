class CreateExpenses < ActiveRecord::Migration
  def change
    create_table :expenses do |t|
      t.decimal :amount, precision: 8, scale: 2
      t.date :date
      t.text :description
      t.string :name

      t.timestamps
    end
  end
end
