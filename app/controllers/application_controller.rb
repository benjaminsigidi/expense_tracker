class ApplicationController < ActionController::Base
  protect_from_forgery

private
  def current_user
    return @current_user if defined?(@current_user)
    @current_user = User.find_by_id(cookies.signed[:user_id]) if cookies.signed[:user_id].present?
  end
  helper_method :current_user

  def require_user
    if current_user.blank?
      redirect_to :login
    end
  end
end
