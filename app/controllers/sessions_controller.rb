class SessionsController < ApplicationController
  def create
    auth = request.env["omniauth.auth"]
    if user = User.find_by_provider_and_uid(auth['provider'], auth['uid'])
      cookies.signed.permanent[:user_id] = user.id
      redirect_to :expenses
    else
      render text: "<pre>Not authenticated. Send Brian the following info:\n  provider: #{auth['provider']}\n  uid: #{auth['uid']}</pre>"
    end
  end
end
