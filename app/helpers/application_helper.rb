module ApplicationHelper
  def include_manifest!
    @include_manifest = true
  end

  def include_manifest?
    !!@include_manifest
  end
end
