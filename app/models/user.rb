class User < ActiveRecord::Base
  attr_accessible :name, :provider, :uid

  has_many :expenses_created, class_name: "Expense", dependent: :nullify, foreign_key: :created_by_id
  has_many :expenses_updated, class_name: "Expense", dependent: :nullify, foreign_key: :updated_by_id
end
