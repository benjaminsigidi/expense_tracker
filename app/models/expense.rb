class Expense < ActiveRecord::Base
  attr_accessible :amount, :date, :description, :name

  belongs_to :created_by, class_name: "User"
  belongs_to :updated_by, class_name: "User"

  before_validation :set_default_values, on: :create

  validates :amount, :created_by, :date, :name, :updated_by, presence: true

  def date=(d)
    d = d.to_i if d.is_a?(String) && d =~ /\d+/
    d = Time.at(d).to_date if d.is_a?(Numeric)
    super
  end

private
  def set_default_values
    self.date ||= Date.today
  end
end
